<?php
/*
Plugin Name: Custom Post Types, Taxonomies and Shortcodes
Plugin URI: -
Description: Register all necessary Custom Post Types, Taxonomies and Shortcodes
Version: 0.5
Author: Theme Owner
Author URI:
License: GPL
Copyright: Theme Owner
*/

if( !class_exists('Cptts') ):

class Cptts
{
	private $json;
	function __construct() {

		$this->json = $this->get_settings_json();


		add_action( 'admin_init', array( $this, 'load_textdomain' ) );

		add_action('init', array( $this, 'create_post_types'));
		add_action('init', array( $this, 'create_shorcodes'));

	}

	function load_textdomain() {
		load_plugin_textdomain( 'cptts', false, 'cptts/languages' );
	}

	public function create_post_types()
	{

		if(!empty($this->json)) {

			if($this->json->create_post_type){

				foreach( $this->json->create_post_type as $name => $post_type ){

					$labels = array(
					'name' => _x($post_type->label_single, 'post type general name', 'cptts-admin'),
					'singular_name' => _x($post_type->label_single, 'post type singular name', 'cptts-admin'),
					'add_new' => _x('Add New', $post_type->label_single, 'cptts-admin'),
					'add_new_item' => __('Add New '.$post_type->label_single, 'cptts-admin'),
					'edit_item' => __('Edit '.$post_type->label_single, 'cptts-admin'),
					'new_item' => __('New '.$post_type->label_single, 'cptts-admin'),
					'all_items' => __('All '.$post_type->label_multi, 'cptts-admin'),
					'view_item' => __('View '.$post_type->label_single, 'cptts-admin'),
					'search_items' => __('Search '.$post_type->label_multi, 'cptts-admin'),
					'not_found' => __('No '.$post_type->label_multi.' found', 'cptts-admin'),
					'not_found_in_trash' => __('No '.$post_type->label_multi.' found in Trash', 'cptts-admin'),
					'parent_item_colon' => '',
					'menu_name' => __($post_type->menu_name, 'cptts-admin')
					);

					$arg_slug = strtolower($post_type->label_single);

					if( isset($post_type->hierarchical) ){
						$arg_hierarchical = $post_type->hierarchical;
					}else{
						$arg_hierarchical = false;
					}

					if( isset($post_type->has_archive) ){
						$arg_has_archive = $post_type->has_archive;
					}else{
						$arg_has_archive = false;
					}

					if( isset($post_type->supports) ){
						$arg_supports = $post_type->supports;
					}else{
						$arg_supports = array('title', 'editor', 'thumbnail','page-attributes');
					}

					if( isset($post_type->with_front) ){
						$arg_with_front = $post_type->with_front;
					}else{
						$arg_with_front = false;
					}



					$args = array(
						'labels' => $labels,
						'public' => true,
						'publicly_queryable' => true,
						'show_ui' => true,
						'show_in_menu' => true,
						'query_var' => true,
						'rewrite' => array('slug' => _x($post_type->slug, 'URL slug', 'cptts-admin'), 'with_front' => $arg_with_front),
						'capability_type' => 'post',
						'has_archive' => $arg_has_archive,
						'hierarchical' => $arg_hierarchical,
						'menu_position' => null,
						'supports' => $arg_supports
						);

					register_post_type($name, $args);

					if( $post_type->tax != false ){

							$this->create_taxonomies($name, $post_type);

					}

				}
			}
		} else {

			add_action( 'admin_notices', array( $this, 'json_error') );

		}
	}

	public function create_taxonomies($post_name, $post)
	{
		foreach($post->tax[0] as $tax_name => $taxonomy)
		{

			$tax_labels = array(
				'name' => _x( $taxonomy->tax_multi, 'taxonomy general name' ),
				'singular_name' => _x( $taxonomy->tax_single, 'taxonomy singular name' ),
				'search_items' =>  __( 'Search '.$taxonomy->tax_multi ),
				'all_items' => __( 'All '.$taxonomy->tax_multi ),
				'parent_item' => __( 'Parent '.$taxonomy->tax_single ),
				'parent_item_colon' => __( 'Parent '.$taxonomy->tax_single.':' ),
				'edit_item' => __( 'Edit '.$taxonomy->tax_single ),
				'update_item' => __( 'Update '.$taxonomy->tax_single ),
				'add_new_item' => __( 'Add New '.$taxonomy->tax_single ),
				'new_item_name' => __( 'New '.$taxonomy->tax_single.' Name' ),
				'menu_name' => __( $taxonomy->tax_single )
				);

				if( isset($taxonomy->hierarchical) )
				{
					$tax_hierarchical = $taxonomy->hierarchical;
				}
				else
				{
					$tax_hierarchical = true;
				}

				if( isset($post_type->with_front) ){
						$arg_with_front = $post_type->with_front;
					}else{
						$arg_with_front = false;
					}

				register_taxonomy($tax_name, array($post_name), array(
					'hierarchical' => $tax_hierarchical,
					'labels' => $tax_labels,
					'show_ui' => true,
					'query_var' => true,
					'rewrite' => array('slug' => _x($post_type->slug, 'URL slug', 'cptts-admin'), 'with_front' => $arg_with_front),
				 ));
		  }
	}

	public function create_shorcodes()
	{
		add_shortcode('c_button', array($this, 'custom_button'));
		add_shortcode('clear', array($this, 'clear_func'));
		add_shortcode('tabs', array($this, 'tabs_func'));
		add_shortcode('tab', array($this, 'tab_func'));
		add_shortcode('button', array($this, 'button_shortcode'));
		add_shortcode('accordion', array($this, 'accordion_func'));
		add_shortcode('slideshow', array($this, 'gallery_func'));
		add_shortcode('line', array($this, 'line_func'));
		add_shortcode('s', array($this, 'span_func'));
	}

	function custom_button($atts) {
		extract(shortcode_atts(array(
			'href' => '#',
			'color' => '',
			'label' => __('Anchor', "CampsAiry&Louise"),
			), $atts));

		return "<a href='$href' class='bt $color'>$label</a>";
	}

	function line_func($atts, $content = "") {
		return "<div class='line'></div>";
	}

	function span_func($atts, $content = "") {
		return "<span>".$content."</span>";
	}

	function button_shortcode($atts,$content = null)
	{

		extract(shortcode_atts(array(
			'url' => '#',
			'type' => ''
			), $atts));

		$params = array ();

		if(!array_key_exists('url', $atts)) {

			foreach($atts as $text) {
				$tmp = explode('=', $text);
				$params[$tmp[0]] = preg_replace('/\"|\'/','', $tmp[1]);
			}
			$params['type'] .= ' '.preg_replace('/\"|\'/','', $atts[2]);

			extract(
				shortcode_atts(
					array(
						'url' => '',
						'type' => ''
						), $params));
		}

		return "<a href='$url' class='button $type'>$content</a>";
	}

	function tabs_func($atts, $content = null)
	{

		extract(shortcode_atts(array(
			'titles' => ''
		), $atts));

		$titles = explode(",", $titles);
		$html = '<div class="tabs">';

			$html .= '<ul class="tab-head hidden-xs">';
			$i=1;
			foreach($titles as $title):
				if ($i == 1) {
					$html .= '<li class="active"><a href="#" data-tab="tabs-'.$i.'">'.trim($title).'</a></li>';
				}
				else {
					$html .= '<li><a href="#" data-tab="tabs-'.$i.'">'.trim($title).'</a></li>';
				}
				$i++;
			endforeach;

			$html .= '</ul>';
			$html .= do_shortcode($content);
		$html .= '</div>';
		return $html;
	}

	function tab_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'id' => '',
			'title' => ''
		), $atts));

		$html = '<div class="tab-trigger-wrapper visible-xs"><a href="#" class="tab-trigger">'.$title.'</a></div>';

		$html .= '<div id="tabs-'.$id.'" class="tab-body">';

			$html .= do_shortcode($content);

		$html .= '</div>';
		return $html;
	}

	function gallery_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'id' => '',
			'align' => '',
			'fullscreen' => '#'
			), $atts));

		if (have_rows('gallery_add_slide', $id)) :

			if ($fullscreen == 'true') {

				$html .= "<div class='slider-wrapper full'><ul class='bxslider'>";

			}

			else {

				$html .= "<div class='slider-wrapper align".$align."'><ul class='bxslider'>";

			}

			while(have_rows('gallery_add_slide', $id)) : the_row();

			$imgId = get_sub_field('gallery_slide');
			$imgSrc = wp_get_attachment_image_src($imgId, 'default-slider-size');
			$imgAlt = get_post_meta($imgId, '_wp_attachment_image_alt', true);
			$slideCaption = get_sub_field('slide_caption');


			$html .= "<li style='background-image: url(".$imgSrc[0].");'><span>".$slideCaption."</span><img src=".$imgSrc[0]." alt=".$slideCaption."></li>";

			endwhile;

			$html .= "</ul><span class='slide-caption'></span></div>";

			endif;

			return $html;


		}

		function accordion_func($atts, $content = null)
		{

			extract(shortcode_atts(array(
				'title' => ''
				), $atts));

			return "<div class='accordion'><div class='accordion-header'>$title</div><div class='accordion-content'>$content</div></div>";

		}

	function clear_func($atts, $content = "")
	{

		return "<div class='clearfix'></div>";

	}

	function get_settings_json()
	{

		$url = dirname(__FILE__) . '/settings.json';
		$json = file_get_contents($url);
		$json = preg_replace("/\/\*(?s).*?\*\//", "", $json);
		$json = json_decode($json);

		return $json;

	}

	public function json_error() {
		?>
		<div class="error">
			<p><?php _e( 'Settings.json in plugins/cptts is invalid.', 'themeName-admin' ); ?></p>
		</div>
		<?php
	}

}

new Cptts();

endif; // class_exists check
